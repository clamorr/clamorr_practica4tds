package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import main.Persona;

import org.junit.After;
import org.junit.Test;

import main.ColaAmigos;

public class ColaAmigosTest {

	private ArrayList<Persona> amigos = new ArrayList<Persona>();
	private ArrayList<Persona> amigos1 = new ArrayList<Persona>();
	private ArrayList<Persona> conocidos = new ArrayList<Persona>();
	private ArrayList<Persona> conocidos1 = new ArrayList<Persona>();
	private Persona p = new Persona("Ana",amigos,conocidos);
	private Persona p1 = new Persona("Clara",amigos1,conocidos1);
	
	@After
	public void tearDown() throws Exception{
		ColaAmigos cola = null;
	}
	
	/**
	 * Crea una nueva cola de amigos
	 */
	public void testCreaColaAmigos(){
		ColaAmigos cola = new ColaAmigos();
	}
	
	/**
	 * No puede crear una cola de tamaño negativo
	 */
	@Test (expected=AssertionError.class)
	public void testNoCreaColaNegativa(){
		ColaAmigos c = new ColaAmigos(-1);
	}
	
	/**
	 * Un amigo puede colar a otro si reservo para el
	 */
	public void testAmigoColaSiReserva(){
		ColaAmigos cola = new ColaAmigos();
		p.addAmigo(p1);
		cola.reservar(p, 1, amigos);
		assertTrue(p.getAmigos().contains(p1));
	}
	
	/**
	 * No puede colar a un amigo por el que no se reservo
	 */
	@Test (expected=AssertionError.class)
	public void testAmigoColaNoReseva(){
		ColaAmigos cola = new ColaAmigos();
		p.addAmigo(p1);
		cola.addAmigoCola(p, p1);
	}
	
	/**
	 * No puede entrar en la cola si ya se encuentra dentro de ella
	 */
	@Test (expected=AssertionError.class)
	public void testNoEntraYaDentro(){
		ColaAmigos cola = new ColaAmigos();
		cola.addSinAmigoCola(p);
		cola.addSinAmigoCola(p);
	}
	
	/**
	 * No puede colar alguien que ya ha sido colado
	 */
	 @Test (expected=AssertionError.class)
	public void testNoCuelaYaColado(){
		 ColaAmigos cola = new ColaAmigos();
		cola.addSinAmigoCola(p);
		p.setColado(true);
		cola.addSinAmigoCola(p);
		
	}
	 
	/**
	 * No puede entrar en la cola si ya salio de ella
	 */
	 @Test (expected=AssertionError.class)
	public void testNoEntraSalio(){
		 ColaAmigos cola = new ColaAmigos();
		cola.addSinAmigoCola(p);
		cola.sacarCola(p);
		cola.addSinAmigoCola(p);
	}
	
	/**
	 * No puede reservar para alguien que no sea su amigo
	 */
	 @Test (expected=AssertionError.class)
		public void testNoReservaNoAmigo(){
		 ColaAmigos cola = new ColaAmigos();
		 ArrayList<Persona> noAmigo = new ArrayList<Persona>();
		noAmigo.add(p1);
		cola.reservar(p, 1,noAmigo );
		}
	 
	/**
	* No puede reservar null
	*/
	 @Test (expected=NullPointerException.class)
	 public void testNoReservaNull(){
		 ColaAmigos cola = new ColaAmigos();
			cola.reservar(p, 1, null);
	}
	 
	/**
	 * No puede colar a un amigo si no se encuentra en la cola
	 */
	@Test (expected=NullPointerException.class)
	public void testNoAmigoCola(){
		ColaAmigos cola = new ColaAmigos();
		cola.addAmigoCola(p, p1);
	}
	
	/**
	 * Se debe de confirmar que desea colar a un amigo
	 */
	public void testConfirmaColar(){
		ColaAmigos cola = new ColaAmigos();
		assertFalse(cola.confirmar(p,p1));
	}
	
	/**
	 * El amigo colado debe estas en la posicion de delante del amigo que lo ha colado
	 */
	public void testAmigoColarDelante(){
		ColaAmigos cola = new ColaAmigos();
		p.setReserva(1);
		cola.addSinAmigoCola(p);
		int posicion = cola.getPosicionAmigo(p);
		cola.addSinAmigoCola(p1);
		assertEquals(posicion,cola.getPosicionAmigo(p));
		assertEquals(cola.getPosicionAmigo(p1),cola.getPosicionAmigo(p));
	}
	
	/**
	 * No puede colar a un amigo a una posicion detra suya
	 */
	@Test (expected=AssertionError.class)
	public void testAmigoNoColarDetras(){
		ColaAmigos cola = new ColaAmigos();
		p.setReserva(1);
		cola.addSinAmigoCola(p);
		cola.addAmigoCola(p1, p);
	}
	
	/**
	 * Debe atender al primero que se encuentre en la cola
	 */
	public void testAtiendePrimero(){
		ColaAmigos cola = new ColaAmigos();
		cola.addSinAmigoCola(p);
		cola.addSinAmigoCola(p1);
		assertEquals(cola.getCola().get(0).getNombre(),p.getNombre());
	}
	
	/**
	 * No debe atender a alguien que no sea el primero
	 */
	@Test (expected=AssertionError.class)
	public void testNoAtiendePrimero(){
		ColaAmigos cola = new ColaAmigos();
		cola.addSinAmigoCola(p);
		cola.addSinAmigoCola(p1);
		cola.atender(p1);
	}
	
	/**
	 * No puede atender al primero si es null
	 */
	@Test (expected=NullPointerException.class)
	public void testNoAtiendePrimeroNull(){
		ColaAmigos cola = new ColaAmigos();
		Persona p2 = null;
		cola.addSinAmigoCola(p2);
		cola.atender(p2);
	}
	
	
	/**
	 * Debe salir de la cola el amigo atendido
	 */
	public void testSacaColaAtendido(){
		ColaAmigos cola = new ColaAmigos();
		cola.addSinAmigoCola(p);
		cola.addSinAmigoCola(p1);
		int tam =cola.getSize();
		assertTrue(cola.isAtendido(p));
		cola.sacarCola(p);
		assertEquals(tam, cola.getSize()+1);	
	}
	
	/**
	 * No debe salir de la cola alguien no atendido
	 */
	@Test (expected=AssertionError.class)
	public void testNoSacaColaNoAtendido(){
		ColaAmigos cola = new ColaAmigos();
		cola.addSinAmigoCola(p);
		cola.addSinAmigoCola(p1);
		cola.atender(p);
		cola.sacarCola(p1);
		
	}
	
	/**
	 * No debe salir de la cola alguien que no sea el primero
	 */
	@Test (expected=AssertionError.class)
	public void testNoSacaColaNoPrimero(){
		ColaAmigos cola = new ColaAmigos();
		cola.addSinAmigoCola(p);
		cola.addSinAmigoCola(p1);	
		cola.sacarCola(cola.getCola().get(1));
		
	}
	
}
