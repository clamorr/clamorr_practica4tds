package test;

import java.util.ArrayList;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import main.ColaAmigos;
import main.Persona;

public class PersonaTest {
	
	private ArrayList<Persona> amigos = new ArrayList<Persona>();
	private ArrayList<Persona> conocidos = new ArrayList<Persona>();
	private ArrayList<Persona> listaAmigosClara = new ArrayList<Persona>();
	private ArrayList<Persona> listaConocidosClara = new ArrayList<Persona>();
	private  ColaAmigos ca = new ColaAmigos();
	


	@After
	public void tearDown() throws Exception{
		this.amigos = null;
		this.conocidos = null;
		Persona persona = null;
	}
	
	/**
	 * Crea una nueva persona
	 */
	public void testCreaPersona(){
		Persona p= new Persona("Ana",amigos,conocidos);
		assertEquals(p.getNombre(), "Ana");
		assertEquals(p.getAmigos(),amigos);
		assertEquals(p.getConocidos(),conocidos);
	}
	
	/**
	 * No crea una nueva persona que no tenga nombre
	 */
	@Test (expected=IllegalArgumentException.class)
	public void testNoCreaPersonaSinNombre(){
		new Persona(null,amigos,conocidos);
		
	}
	
	/**
	 * La reserva no puede ser para mas de 10
	 */
	 @Test (expected=AssertionError.class)
	public void testNoReservaMas10() {
		 Persona p= new Persona("Ana",amigos,conocidos);
		 p.setReserva(11);
    }
	
	/**
	 * No se puede reservar para un numero negativo de personas
	 */
	 @Test (expected=AssertionError.class)
	public void testNoReservaNegativos() {
		 Persona p= new Persona("Ana",amigos,conocidos);
		p.setReserva(-1);
    }
		/**
		 * No puede reservar para mas personas que amigos tenga
		 */
	 @Test (expected=AssertionError.class)
	public void testReservaAmigos(){
		 Persona p= new Persona("Ana",amigos,conocidos);
		p.setReserva(p.getAmigos().size());
	}
	
	/**
	 * Comprobar que el numero de reserva es mayor que los amigos que quedan por colar
	 */
	public void testAmigosRestantes(){
		Persona p= new Persona("Ana",amigos,conocidos);
		assertTrue(p.getReserva()>p.amigosPorColar());
		
	}
	
	/**
	 * Comprobar que no le quedan mas amigos por colar que los iniciales
	 */
	@Test (expected=AssertionError.class)
	public void testNoAmigosRestantesSuperior(){
		Persona p= new Persona("Ana",amigos,conocidos);
		assertTrue(p.getReserva()<p.amigosPorColar());}
	
	/**
	 * Comprobar que no le quedan amigos negativos por colar
	 */
	@Test (expected=AssertionError.class)
	public void testNoAmigosRestantesNegativos(){
		Persona p= new Persona("Ana",amigos,conocidos);
		assertTrue(p.amigosPorColar()>0);}
	
	/**
	 * No le quedan mas de 10 amigos por colar
	 */
	 @Test (expected=AssertionError.class)
	public void testNoAmigosRestantesMas10(){
		 Persona p= new Persona("Ana",amigos,conocidos);
		 assertTrue(p.amigosPorColar()>10);}
	
	/**
	 * No puede ser amigo de si mismo
	 */
	 @Test (expected=AssertionError.class)
		public void testNoReservaASiMismo(){
		 Persona p= new Persona("Ana",amigos,conocidos);
		 p.addAmigo(p);
	}
		
	/**
	 * Añadir un amigo a la lista de amigos de la persona
	 */
		public void testAddAmigo(){
			Persona p= new Persona("Ana",amigos,conocidos);
			Persona amigo = new Persona("Clara",listaAmigosClara,listaConocidosClara);
			p.addAmigo(amigo);
			assertTrue(p.getAmigos().contains(amigo));
		}
		
		/**
		 * No añade a la lista de amigos un amigo ya añadido
		 */
		@Test (expected=AssertionError.class)
		public void testNoAddAmigoYaAdded(){
			Persona p= new Persona("Ana",amigos,conocidos);
			Persona amigo = new Persona("Clara",listaAmigosClara,listaConocidosClara);
			p.addAmigo(amigo);
			p.addAmigo(amigo);
		}
		
		/**
		 * No puede añadir un amigo null
		 */
		@Test (expected=NullPointerException.class)
		public void testNoAddAmigoNull(){
			Persona p= new Persona("Ana",amigos,conocidos);
			p.addAmigo(null);
		}
		 
		/**
		 * No añade a la lista de amigos una persona que no sea conocido
		 */
		@Test (expected=AssertionError.class)
		public void testNoAddAmigoNoConocido(){
			Persona p= new Persona("Ana",amigos,conocidos);
			Persona amigo = new Persona("Clara",listaAmigosClara,listaConocidosClara);
			p.addConocido(amigo);
			p.addAmigo(amigo);
		}
		
		/**
		 * No puede añadir a la lista de amigos a si mismo
		 */
		@Test (expected=AssertionError.class)
		public void testNoAddAmigoSiMismo(){
			Persona p= new Persona("Ana",amigos,conocidos);
			p.addAmigo(p);
		}
		
		/**
		 * Dejar de ser amigo con una persona
		 */
		public void testDejarAmistad(){
			Persona p= new Persona("Ana",amigos,conocidos);
			Persona amigo = new Persona("Clara",listaAmigosClara,listaConocidosClara);
			p.addAmigo(amigo);
			int amigos = p.getAmigos().size();
			p.amistadRota(amigo);
			assertEquals(p.getAmigos().size(),amigos);
			assertTrue(p.getAmigos().contains(amigo));
		}
		
		/**
		 * No puede dejar de ser amigo de alguien que no lo es
		 */
		@Test (expected=AssertionError.class)
		public void testNoDejaAmistadNoAmigo(){
			Persona p= new Persona("Ana",amigos,conocidos);
			Persona amigo = new Persona("Clara",listaAmigosClara,listaConocidosClara);
			p.amistadRota(amigo);
		}
		

}
