package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	PersonaTest.class,
	ColaAmigosTest.class,
	ColaAmigosTestMock.class
})
public class AllTests {
}

