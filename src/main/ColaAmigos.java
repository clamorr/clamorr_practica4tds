package main;

import java.util.ArrayList;

public class ColaAmigos {
	
	private ArrayList<Persona> cola ;
	private ArrayList<Persona> reservados;
	private boolean confirma;
	private int posicion;
	private int size;
	
	/**
	 * Constructor de la clase sin parametros
	 */
	public ColaAmigos(){
		this.cola=new ArrayList<Persona>();
	}
	
	/**
	 * Constructor de la clase con el tamaño como parametro
	 * @param size
	 */
	public ColaAmigos(int size){
		if(size>=0) {
			this.cola=new ArrayList<Persona>(size);
			setSize(size);
		}
		else throw new AssertionError(); 
	}
	
	/**
	 * Devuelve la cola
	 * @return
	 */
	public ArrayList<Persona> getCola() {
		return this.cola;
	}
	
	/**
	 * Devuelve el tamaño de la cola
	 * @return
	 */
	public int getSize(){
		this.size = cola.size();
		return this.size;
	}
	
	/**
	 * Da tamaño a la cola
	 * @param size
	 */
	public void setSize(int size){
		if(size>=0) this.size=size;
		else throw new AssertionError();
	}
	
	/**
	 * Atiende a una persona siempre que se encuentre en la primera posicion y no sea null
	 * @param p
	 */
	public void atender(Persona p){
		if(cola.indexOf(p)==0&&!p.equals(null)) p.setAtendido(true);
		else throw new AssertionError();
	}
	
	/**
	 * Elimina de la cola a una persona siempre que haya sido atendida
	 * @param p
	 */
	public void sacarCola(Persona p){
		if(isAtendido(p)){
			this.cola.remove(p);
		}
		else throw new AssertionError();
	}
	
	/**
	 * Añade amigo a la cola si ha reservado para el, 
	 * no esta dentro de la cola, 
	 * la persona que le va a colar esta dentro
	 * y no ha estado anteriormente en la cola
	 * @param p
	 * @param amigo
	 */
	public void addAmigoCola(Persona p, Persona amigo){
		if(this.reservados.contains(p) && !cola.contains(amigo) && cola.contains(p) && !p.isColaAnterior()) {
			p.setColaAnterior(true);
			this.cola.add(getPosicionAmigo(p), amigo);
		}
		else throw new AssertionError();
	}
	
	/**
	 * Añade una persona a la cola que no tiene amigo dentro
	 * si esa persona no es null
	 * no esta ya dentro
	 * no ha estado anteriormente en la cola
	 * @param p
	 */
	public void addSinAmigoCola(Persona p){
		if(!p.equals(null) && !cola.contains(p) && !p.isColaAnterior()){
			p.setColaAnterior(true);	
			this.cola.add(cola.size(), p);
		}
		else throw new AssertionError();
	}
	
	/**
	 * Una persona reserva para un numero n de personas -->amigos
	 * @param persona
	 * @param n
	 * @param amigos
	 */
	public void reservar(Persona persona, int n, ArrayList<Persona>amigos){
		if(persona.getAmigos().containsAll(amigos)
				&&!persona.isReservado() 
				&&!amigos.equals(null)){
			persona.setReserva(n);
			for(int i =0;i<amigos.size();i++){
				if(!reservados.contains(amigos.get(i))) this.reservados.add(amigos.get(i));
			}
		}else throw new AssertionError();
		
	}
	
	/**
	 * Nos dice si una persona ha sido atendida o no
	 * @param p
	 * @return
	 */
	public boolean isAtendido(Persona p) {
		return p.isAtendido();
	}
	
	/**
	 * Da el valor atendido a una persona
	 * @param p
	 * @param atendido
	 */
	public void setAtendido(Persona p, boolean atendido) {
		p.setAtendido(atendido);
	}
	
	/**
	 * Una persona confirma amistad con otra
	 * @param p1
	 * @param p2
	 * @return
	 */
	public boolean confirmar(Persona p1, Persona p2){
		this.confirma=true;
		return confirma;
	}
	
	/**
	 * Obtenemos la posicion en la que se encuentra un amigo
	 * @param p
	 * @return
	 */
	public int getPosicionAmigo(Persona p) {
		this.posicion = cola.indexOf(p);
		return posicion;
	}
}
