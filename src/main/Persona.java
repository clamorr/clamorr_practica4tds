package main;

import java.util.ArrayList;



public class Persona {
	
	private String nombre;
	private ArrayList<Persona> amigos;
	private ArrayList<Persona> conocidos;
	private int reserva ;
	private boolean colado;
	private boolean colaAnterior;
	private boolean reservado;

	private int MAXRESERVA =10;
	
	private boolean atendido;
	

	
	/**
	 * Constructor con argumentos
	 * @param nombre
	 * @param amigos
	 * @param conocidos
	 */
	public Persona(String nombre, ArrayList<Persona> amigos, ArrayList<Persona> conocidos){
		if(nombre==null)
			throw new IllegalArgumentException("La Persona debe tener nombre");
		this.nombre = nombre;
		this.amigos=amigos;
		this.conocidos=conocidos;
	}

	/**
	 * Devuelve el nombre de la persona
	 * @return
	 */
	public String getNombre() {
		return this.nombre;
	}


	/**
	 * Devuelve la lista de amigos de la persona
	 * @return
	 */
	public ArrayList<Persona> getAmigos() {
		return this.amigos;
	}

	/**
	 * Setter de la lista de amigos de la persona
	 * @param amigos
	 */
	public void setAmigos(ArrayList<Persona> amigos) {
		this.amigos=amigos;
	}

	/**
	 * Devuelve la lista de conocidos de la persona
	 * @return
	 */
	public ArrayList<Persona> getConocidos() {
		return this.conocidos;
	}

	/**
	 * Setter de los conocidos de la persona
	 * @param conocidos
	 */
	public void setConocidos(ArrayList<Persona> c) {
		this.conocidos=conocidos;
	}
	
	/**
	 * Devuelve el numero de personas que reservo la persona
	 * @return
	 */
	public int getReserva() {
		return this.reserva;
	}
	
	/**
	 * Asigna un numero de reserva a la persona
	 * @param r
	 */
	public void setReserva(int r) {
		if(r<= MAXRESERVA&&r>0){
			setReservado(true);
			this.reserva=r;
		}else
			throw new AssertionError();
	}
	
	/**
	 * Da la etiqueta de colado a la persona
	 * @return
	 */
	public boolean isColado() {
		this.colado=true;
		return colado;
	}
	
	/**
	 * Obtiene si la persona ha sido colada
	 * @param c
	 */
	public void setColado(boolean c) {
		this.colado=true;
	}
	
	/**
	 * Devuelve el numero de amigos que faltan por colar
	 * @return
	 */
	public int amigosPorColar(){
		int amigosPorColar;
		int colados=0;
		ColaAmigos ca = new ColaAmigos();
		for(int i =0;i<ca.getSize();i++){
			if(!getAmigos().contains(ca.getCola().get(i))) {}
			else	colados++;
		}
		amigosPorColar =reserva-colados;
		return amigosPorColar;
		}
	
	/**
	 * Devuelve si ha estado en la cola anteriormente
	 * @return
	 */
	public boolean isColaAnterior() {
		return true;
	}
	
	/**
	 * Da a la persona el valor de que ha estado en la cola anteriormente
	 * @param colaAnterior
	 */
	public void setColaAnterior(boolean colaAnterior) {	
		this.colaAnterior=colaAnterior;
	}
	
	/**
	 * Añade un amigo a la persona
	 * @param p2
	 */
	public void addAmigo(Persona p2){
		if(p2.equals(null))
			throw new AssertionError();
		else{
			if(amigos.contains(p2)&& !conocidos.contains(p2)) amigos.add(p2);
			else
				throw new AssertionError();
		}
	}
	
	/**
	 * Nos dice si hay amistad con una persona
	 * @param p2
	 * @return
	 */
	public boolean isAmistad(Persona p2){
		if(getAmigos().contains(p2))
			return true;
		else return false;
	}
	
	/**
	 * Elimina amistad con una persona
	 * @param p2
	 */
	public void amistadRota( Persona p2){
		if(amigos.contains(p2)) this.amigos.remove(p2);
		else throw new AssertionError();
	}

	/**
	 * Añade a la lista de conocidos a una persona
	 * @param p
	 */
	public void addConocido(Persona p) {
		if(!conocidos.contains(p) && !p.equals(null))
			 this.conocidos.add(p);
	}
	
	/**
	 * Nos dice si una persona ha reservado
	 * @return
	 */
	public boolean isReservado() {
		return reservado;
	}

	/**
	 * Da el valor de reservado a una persona
	 * @param reservado
	 */
	public void setReservado(boolean reservado) {
		this.reservado = reservado;
	}
	
	/**
	 * Nos dice si una persona ha sido atendida
	 * @return
	 */
	public boolean isAtendido() {
		return this.atendido;
	}

	/**
	 * Da el valor de atendido a una persona
	 * @param atendido
	 */
	public void setAtendido(boolean atendido) {
		this.atendido = atendido;
	}

}

