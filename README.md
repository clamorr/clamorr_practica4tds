# README # 
Version 1.0 18/01/2016


  NOTAS GENERALES DE USO
  ----------------------

Este repositorio incluye un proyecto denominado “ColaAmigos” la cual nos permite desarrollar una serie de acciones explicadas en el punto siguiente. Además, mediante Maven, se realizarán un conjunto de acciones automáticamente.

Se han utilizado objetos mock, usando para su implementación la librería EasyMock. 

Los detalles de la última versión se pueden encontrar en el archivo Pom.xml. Ahora se encuentra en 0.0.1-SNAPSHOT.
La version de JUnit es la 3.8.1 y es JDK 7.

  ¿QUÉ ES?
  -----------

“ColaAmigos” describe una cola en la que se debe pedir la vez para entrar y además se puede reservar para colar a un número concreto de amigos.
Cada persona cuenta con un conjunto de amigos y de conocidos(estos últimos pueden llegar a ser amigos).
Si una persona se encuentra en la cola y ha reservado para un amigo, cuando éste último llegue lo puede colar en la posición anterior a la suya. 

Hay un conjunto de restricciones que se deben cumplir:
	. No se puede reservar para más de 10 persona
	. No se puede colar a más amigos de los reservados
	. Un amigo colado no puede colar a nadie
	. Una persona que sale de la cola no puede volver a entrar

Estas restricciones y otros factores importantes se han probado mediante la utilización de un conjunto de Test.El diseño se ha basado en TDD.
También se han realizado pruebas de caja blanca mediante el uso de objetos mock con el uso de EasyMock.

El proyecto ha seguido una estructura arquetipo maven- archetype-quickstart, desarrollado en los archivos pom.xml y build.xml.

  CONFIGURACIÓN Y EJECUCIÓN
  --------------------------

Se necesita un ordenador con Java 7 y JDK 7 instalado para poder ejecutar el proyecto, ya sea a través de un IDE como eclipse o directamente desde un terminal(o similar), con un script de ant.

Para ello, se debe descargar todos los ficheros fuente, documentación, etc. e introducirlos en un proyecto creado en el IDE o correrlos con el script desde el directorio en el que se encuentren los ficheros.

Los ficheros Java se encuentran divididos en dos carpetas, una con las clases de objetos y otras con los tests. La primera de ellas es src/main/java/ y la segunda src/test/java/. Todas las clases y test cuentan con su propio javadoc para mayor facilidad de comprensión. 

Si nos hemos decantado por la opción de utilizar un IDE, debemos tener Maven en dicho entorno e importar el proyecto Maven. Ejecutaremos la clase AllTest.java si queremos ver el resultado de todos los test implementados.

Si usamos un terminal, nos situaremos en el directorio donde se encuentra el proyecto (con el comando ‘cd directorio’) y con el archivo “build.xml” podremos realizar todas las actividades que queramos (limpiar, compilar, ejecutar o documentar).


  INFORMACIÓN TEST
  -----------------
Existen dos ficheros con test para probar las diferentes clases.
Una de ellas es PersonaTest, que prueba todos los detalles y las restricciones de la clase Persona. La otra es ColaAmigosTestMock la cual usa mock objects para probar el correcto funcionamiento de la clase ColaAmigos.

Los test los podemos ejecutar independientemente o todos a la vez, mediante una clase llamada AllTest.java que proporciona la ejecución simultánea de todos ellos.


  CONTACTO
  --------
Propietarios y administradores:
		Clara Morrondo Merino (clamorr)